# Занятие №1 Модуль №4

В репозитории занятия размещен проект с примером занятия.

## Задача:

1. Установить Vue CLI
2. Создать тестовый проект
3. Создать один компонент с картинкой и подписью
4. Разместить его на главной странице в 3х экземплярах с разным содержимым.
5. Прислать ссылку на ваш репозиторий
